//
//  SocketManager.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import UIKit

struct NoConnectionToServerAlert {
    
    private let alert: UIAlertController
    private let currentViewController : UIViewController
    
    private let title = "Нет подключения к сети"
    private let message = "Пожалуйста, проверьте свое подключение к интернету."
    
    init() {
        let window = UIApplication.shared.keyWindow
        self.currentViewController = UIViewController.getVisibleViewController(window?.rootViewController)!
        
        self.alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.addTryAgainButton(to: alert)
        self.addLogOutButton(to: alert)
    }
    
    private func addLogOutButton(to alertController: UIAlertController) {
        let showSettings = UIAlertAction(title: "Выйти", style: .default) { (alert) in
            UserManager.deleteCurrentUser()
            Router.Login.showAsRoot(fromViewController: self.currentViewController)
        }
        alertController.addAction(showSettings)
    }
    
    private func addTryAgainButton(to alertController: UIAlertController) {
        let tryAgainButton = UIAlertAction(title: "Повторить", style: .default) { (_) in
            Socket.shared.open()
        }
        alertController.addAction(tryAgainButton)
    }
    
    func show() {
       self.currentViewController.present(alert, animated: true, completion: nil)
    }
}



