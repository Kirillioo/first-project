//
//  JSQMessagesCollectionViewCellCustom.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class JSQMessagesCollectionViewCellIncomingCustom : JSQMessagesCollectionViewCellIncoming {
    
    @IBOutlet weak var timeLabel: UILabel!
}

class JSQMessagesCollectionViewCellOutgoingCustom : JSQMessagesCollectionViewCellOutgoing {
    
    @IBOutlet weak var timeLabel: UILabel!
}
