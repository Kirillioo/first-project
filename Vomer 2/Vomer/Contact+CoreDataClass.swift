//
//  Contact+CoreDataClass.swift
//  
//
//
//

import Foundation
import CoreData

@objc(Contact)
public class Contact: NSManagedObject {
    
    var fullNumber : String {
        return self.code + self.number
    }
    
    func setProperties(withData: Dictionary<String, AnyObject>) {
        let date = TimeInterval(withData["sort"] as? String ?? "")
        self.dateLastMessage = NSDate(timeIntervalSince1970: date ?? 0)
        self.city = withData["city"] as? String ?? ""
        self.code = withData["code"] as? String ?? ""
        self.country = withData["country"] as? String ?? ""
        self.email = withData["email"] as? String ?? ""
        self.fio = withData["fio"] as? String ?? ""
        self.path = withData["path"] as? String ?? ""
        self.lastMessage = withData["d_lastt"] as? String ?? ""
        self.lastVisit = withData["lastvisit"] as? String ?? ""
        self.number = withData["number"] as? String ?? ""
        self.userID = withData["userID"] as? Int64 ?? -1
        self.online = withData["online"] as? Int64 ?? -1
        self.dialogID = withData["dialogID"] as? Int64 ?? -1
        self.balance = withData["balance"] as? Int64 ?? -1
        self.countNewMessages = withData["countnew"] as? Int64 ?? -1
        self.dType = withData["d_type"] as? Int64 ?? -1
        self.dTitle = withData["d_title"] as? String ?? ""
        self.dPath = withData["d_path"] as? String ?? ""
        self.phone = withData["phone"] as? String ?? ""
    }
}
