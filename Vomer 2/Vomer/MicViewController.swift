//
//  MicViewController.swift
//  Vomer
//
//  Created by Lora Kucher on 03.12.16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class MicViewController: UIViewController, UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
   navigationItem.titleView = setTitle(title: "Людмила", subtitle: "online")

  

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            navigationController?.setNavigationBarHidden(true, animated: true)
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    func setTitle(title:String, subtitle:String) -> UIView {
        let titleLabel = UILabel(frame: CGRect(x: -30, y: -2, width: 0, height: 0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x: -30, y: 18, width: 0, height: 0))
        subtitleLabel.backgroundColor = UIColor.clear
        
        subtitleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6971738674)
        subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        return titleView
    }
    
  
    
    @IBAction func didTapDown(_ sender: UIButton) {
        sender.setImage(UIImage(named: "microphone red"), for: .normal)
    
    }
    @IBAction func didTapInside(_ sender: UIButton) {
        sender.setImage(UIImage(named: "microphone yellow"), for: .normal)
    }
}



