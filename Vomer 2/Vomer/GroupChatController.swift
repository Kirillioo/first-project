//
//  GroupChatController.swift
//  Vomer
//
//  Created by Lora Kucher on 06.12.16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

protocol SelectDeselectDelegate: class {
    func someoneDidSelect(userID: String)
    func someoneDidDeselect(userID: String)
}

class GroupChatController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    fileprivate var contacts = [Contact]()
    
    weak var selectDeselectDelegate : SelectDeselectDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.setEditing(true, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContactsCell), name: .myNotificationReloadContacts, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.contacts = ContactManager.allContacts
        self.tableView.reloadData()
    }

    // MARK: - UITableView DataSourse.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as! GroupContactsTableViewCell
        
        let contact = contacts[indexPath.item]
        
        cell.nameLabel.text = contact.fio
        cell.phoneLabel.text = contact.fullNumber
        cell.photo.sd_setImage(with: URL.init(string: ContactManager.imageUrlSmall + contact.path)!, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))

        return cell
    }
    
    // MARK: - UITableView Delegate.
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userID = self.contacts[indexPath.item].userID
        self.selectDeselectDelegate?.someoneDidSelect(userID: String(userID))
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let userID = self.contacts[indexPath.item].userID
        self.selectDeselectDelegate?.someoneDidDeselect(userID: String(userID))
    }
    
    // MARK: - UpdateContactsNotification.
    
    func reloadContactsCell() {
        self.contacts = ContactManager.allContacts
        self.tableView.reloadSections( IndexSet.init(integer: 0) , with: .fade)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
