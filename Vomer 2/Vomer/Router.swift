//
//  Router.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

struct Router {
    
    struct MainTabBar {
        static func setRoot(fromViewController viewController: UIViewController?) {
            let newViewController = UIStoryboard(name: "MainTabBar", bundle: nil).instantiateInitialViewController()
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window!.rootViewController = newViewController
            viewController?.present(newViewController!, animated: true, completion: nil)
        }
    }
    
    struct Login {
        static func show(fromViewController viewController: UIViewController?) {
            let newViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginController") as! LoginController
            viewController?.navigationController?.pushViewController(newViewController, animated: true)
        }
        
        static func showAsRoot(fromViewController viewController : UIViewController?) {
            let newViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginController") as! LoginController
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window!.rootViewController = newViewController
            viewController?.present(newViewController, animated: true, completion: nil)
        }
        
        struct WebPhone {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "WebPhone", bundle: nil).instantiateViewController(withIdentifier: "WebPhoneController") as! WebPhoneController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        
        struct ForgotPIN {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "ForgotPIN", bundle: nil).instantiateViewController(withIdentifier: "ForgotPINController") as! ForgotPINController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
    }
    
    struct UserProfile {
        struct Settings {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsTableController") as! SettingsTableController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        
        struct InviteFriends {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "InviteFriends", bundle: nil).instantiateViewController(withIdentifier: "InviteFriendsController") as! InviteFriendsController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        
        struct Gallery {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "GalleryController") as! GalleryController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        
        struct  Language {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "Language", bundle: nil).instantiateViewController(withIdentifier: "LanguageController") as! LanguageController
        viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }

        }
        
        struct MyProfile {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "MyProfile", bundle: nil).instantiateViewController(withIdentifier: "MyProfileTableController") as! MyProfileTableController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
    }
    
    struct Chat {
        static func show(fromViewController viewController: UIViewController?, contactModel: Contact) {
            let newViewController = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            newViewController.contactModel = contactModel
            viewController?.navigationController?.pushViewController(newViewController, animated: true)
        }
        
        struct ChatSettings {
            static func show(fromViewController viewController: UIViewController?, contactModel: Contact?) {
                let newViewController = UIStoryboard(name: "ChatSettings", bundle: nil).instantiateViewController(withIdentifier: "ChatSettingsTableController") as! ChatSettingsTableController
                newViewController.contactModel = contactModel
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        
//        struct GroupChat {
//            static func show(fromViewController viewController: UIViewController?) {
//                let newViewController = UIStoryboard(name: "GroupChat", bundle: nil).instantiateViewController(withIdentifier: "GroupChatController") as! GroupChatController
//                viewController?.navigationController?.pushViewController(newViewController, animated: true)
//            }
//        }
        
    }
    
    struct UserDetail {
        static func show(fromViewController viewController: UIViewController?, contactModel: Contact) {
            let newViewController = UIStoryboard(name: "UserDetail", bundle: nil).instantiateViewController(withIdentifier: "UserDetailTableController") as! UserDetailTableController
            newViewController.contactModel = contactModel
            viewController?.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    
    struct Common {
        struct Mic {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "Mic", bundle: nil).instantiateViewController(withIdentifier: "MicViewController") as! MicViewController
                viewController?.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
        struct IncomingCall {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "IncomingCall", bundle: nil).instantiateViewController(withIdentifier: "IncomingCallControllerViewController") as! IncomingCallControllerViewController
                viewController?.present(newViewController, animated: true, completion: nil)
            }
        }
        struct VideoCall {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCallController") as! VideoCallController
                viewController?.present(newViewController, animated: true, completion: nil)
            }
        }
        struct Call {
            static func show(fromViewController viewController: UIViewController?) {
                let newViewController = UIStoryboard(name: "Call", bundle: nil).instantiateViewController(withIdentifier: "CallController") as! CallController
                viewController?.present(newViewController, animated: true, completion: nil)
            }
        }
    }
    
 
    
}
