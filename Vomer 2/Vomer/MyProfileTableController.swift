//
//  MyProfileTableController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/21/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol MyProfileTableControllerDelegate {
    func userChangeInfo(name: String)
}


class MyProfileTableController: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var contactModel : Contact?
    var path : String?
    var hud = MBProgressHUD()
    
    //TODO: - userPhoto
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var fioTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        let photoPath = URL.init(string: ContactManager.imageUrlBig + UserManager.curentUser!.path)!
        self.userPhoto.sd_setImage(with: photoPath, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
        self.numberTextField.text = UserManager.curentUser?.phone
        self.fioTextField.text = UserManager.curentUser?.fio
        self.dateOfBirthTextField.text = UserManager.curentUser?.birth
        self.countryTextField.text = UserManager.curentUser?.country
        self.cityTextField.text = UserManager.curentUser?.city
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            navigationController?.setNavigationBarHidden(true, animated: true)
            tabBarController?.tabBar.isHidden = false
        }
        
        sendInfo()
    }
    
    func sendInfo() {
        if UserManager.isHaveCurrent {
            UserAPI.changeUserData(number: UserManager.curentUser!.fullNumber, pin: "\(UserManager.curentUser!.pin)",
                fio: self.fioTextField.text!, phone: self.numberTextField.text!,
                birth: self.dateOfBirthTextField.text!, country: self.countryTextField.text!,
                city: self.cityTextField.text!, email: UserManager.curentUser!.email,
                path: self.path == nil ? UserManager.curentUser!.path : self.path!, id: "\(UserManager.curentUser!.userID)") { (response) in
                    
                    switch response {
                    case .Success:
                        print(response)
                        UserManager.changeUserInfoWith(type: nil, userID: nil, code: nil, fio: self.fioTextField.text, email: nil, birth: self.dateOfBirthTextField.text, country: self.countryTextField.text, city: self.cityTextField.text, balance: nil, language: nil, path: self.path, newPath: nil, completion: {
                            print(UserManager.curentUser.debugDescription)
                        })
                        
                    case let .Error(message: msg) : print(msg ?? "fuck")
                        
                    }
            }
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text == "" {
            return false
        }
        return true
    }
    
    //MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true

            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerController Delegate.
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.userPhoto.contentMode = .scaleAspectFit
            self.userPhoto.image = image
            
            self.showHud()
            
            UserAPI.uploadUserPhoto(userID: UserManager.curentUser!.userID, image: image, completion: { [unowned self] (path) in
                self.hud.hide(animated: false)
                
                if path != nil {
                    self.path = path!
                } else {
                    _ = SweetAlert().showAlert("Error!", subTitle: "Не получилось загрузить фото на сервер. Пожалуйста, проверте свое соединение с интернетом.", style: .error)
                }
            })
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func showHud() {
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.label.text = "Loading"
    }
    
    

}
