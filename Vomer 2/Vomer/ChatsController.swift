//
//  ChatsController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import SDWebImage

class ChatsController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!

    internal var chats = ContactManager.allContacts
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBarUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContactsCell), name: .myNotificationReloadContacts, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ContactManager.updateAllContacts(nil)
        self.chats = ContactManager.allSingleChats
        self.tableView.reloadData()
        
        if tabBarController?.tabBar.isHidden == true {
            tabBarController?.tabBar.isHidden = false
        }

    }
    
    // MARK: - UITableView DataSource.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as! ChatsTableViewCell
        
        let contact = chats[indexPath.item]
        
        cell.photo.sd_setImage(with: URL.init(string: ContactManager.imageUrlSmall + contact.path)!, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
        cell.nameLabel.text = contact.fio
        cell.lastMessageLabel.text = contact.lastMessage
        cell.timeLastMessageLabel.text = MyDateFormatter.getTimeStringFromDate(contact.dateLastMessage)
        cell.unreadMessagesLabel.text = String(contact.countNewMessages)
        cell.unreadMessagesLabel.isHidden = contact.countNewMessages > 0 ? false : true
        cell.timeLastMessageLabel.isHidden = contact.lastMessage == "" ? true : false
        
        return cell
    }
    
    // MARK: - UITableView Delegate.
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        Router.Chat.show(fromViewController: self, contactModel: chats[indexPath.item])
    }
    
    // MARK: - UpdateContactsNotification.
    
    func reloadContactsCell() {
        self.chats = ContactManager.allSingleChats
        self.tableView.reloadSections( IndexSet.init(integer: 0) , with: .fade)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
    
    
    
    
    

    
}
