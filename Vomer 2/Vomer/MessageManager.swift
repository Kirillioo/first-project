//
//  MessageManager.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation
import MagicalRecord


protocol ChatManagerDelegate: class {
    
    func onNewMessage(message: JSQMessagesMy)
    func onWritesMessage(dialogID: Int, fio: String)
    func onReadMessage(hashMessage: String, dialogID: Int, userID: Int)
    func messageDidSend(myMessage: JSQMessagesMy)
}


struct MessageManager {

    static weak var delegate : ChatManagerDelegate?
    
    //MARK: ----------------------------------- From Socket
    static func getNewMessage(data: Any) {
        if let dataArray = data as? [Dictionary<String, AnyObject>] {
            if let senderID = dataArray[0]["id"] as? Int64 {
                
                MagicalRecord.save({ (localContext) in
                    let message = Message.mr_createEntity(in: localContext)
                    message?.setProperties(withData: dataArray[0])
                })
                
                let jsqMessage = JSQMessagesMy(withData: dataArray[0])

                if senderID == UserManager.curentUser!.userID {
                    self.delegate?.messageDidSend(myMessage: jsqMessage)
                } else {
                    ContactManager.updateAllContacts(nil)
                    self.delegate?.onNewMessage(message: jsqMessage)
                }
            }
        }
    }
    
    static func onWritesMessage(_ data: Any) {
        
        let data = data as! Array<Dictionary<String, AnyObject>>
        let fio = data[0]["fio"] as? String ?? ""
        let dialogID = data[0]["dialog_id"] as? Int ?? -1
        
        self.delegate?.onWritesMessage(dialogID: dialogID, fio: fio)
    }
    
    static func onReadMessage(data: Any) {
        
        let data = data as! Array<Dictionary<String, AnyObject>>
        let hashMessage = data[0]["hash"] as? String ?? ""
        let userID = data[0]["id"] as? Int ?? -1
        let dialogID = data[0]["dialog_id"] as? Int ?? -1
        
        self.delegate?.onReadMessage(hashMessage: hashMessage, dialogID: dialogID, userID: userID)
    }
    
    //MARK: ----------------------------------- To Socket
    
    static func userWhriteMessage(dialogID: Int64) {
        Socket.shared.userWhriteMessage(dialogID: dialogID)
    }
    
    static func userReadMessage(dialogID: Int64, hash: String, userIDCome: Int64) {
        Socket.shared.userReadMessage(dialogID: dialogID, hash: hash, userIDCome: userIDCome)
    }
    
    static func sendMessage(message: JSQMessagesMy, dialogID: Int64) {
        Socket.shared.sendMessage(messageText: message.text, toID: dialogID)
    }
    
    
     //MARK: ------------------------------------ CoreData
    
    static func getAllMessages(_ dialogID: Int64) -> ([JSQMessagesMy]) {
        
        let predicate = NSPredicate(format: "dialogID == %@", String(dialogID))
        let messagesArray = Message.mr_findAllSorted(by: "sorting", ascending: true, with: predicate) as! [Message]
        
        var arrayForVC = [JSQMessagesMy]()
        
        for mess in messagesArray {
            
            if mess.status == 0 && mess.senderID != UserManager.curentUser!.userID {
                self.userReadMessage(dialogID: mess.dialogID, hash: mess.hashMessage,
                                     userIDCome: mess.senderID)
            }
            
            let message = JSQMessagesMy(senderId: String(mess.senderID), displayName: mess.fio, text: mess.text)
            message?.sendStatus = mess.sendStatus
            message?.status = mess.status
            message?.dateString = mess.dateString
            message?.timeString = mess.timeString
            message?.hashMessage = mess.hashMessage
            message?.dialogID = mess.dialogID
            arrayForVC.append(message!)
        }

        return arrayForVC
    }
    
    static func deleteAllMessages() {
        MagicalRecord.save({ (localContext) in
            Message.mr_truncateAll(in: localContext)
        })
    }

    static func updateMessagesInBase(_ dialogID: Int64) {
        self.getAllMessagesFromServer(dialogID: dialogID) { (messages) in
            MagicalRecord.save({ (localContext) in
                
                chekForDeleteMessages(dictionaryMessages: messages, context: localContext)
                chekForNewMessages(dictionaryMessages: messages, context: localContext)
                
            }, completion: { (_, _) in
                NotificationCenter.default.post(name: .myNotificationMessagesUpdateDidEnd, object: nil)
            })
        }
    }
    
    private static func getAllMessagesFromServer(dialogID: Int64, completion: @escaping (_ data: Dictionary<String, AnyObject>) -> ()) {
        Socket.shared.getAllMessages(dialogID: dialogID) { (data) in
            if let array = data as? [Dictionary<String, AnyObject>] {
                if let arrayMessages = array.first {
                    completion(arrayMessages)
                }
            }
        }
    }
    
    private static func chekForDeleteMessages(dictionaryMessages: Dictionary<String, AnyObject>, context : NSManagedObjectContext) {
        
        let arrayUsers = Array(dictionaryMessages.values)
        var hashList = [String]()
        
        arrayUsers.forEach { (obj) in
            let hash = obj["hash"] as? String ?? ""
            hashList.append(hash)
        }
        
        let predicate = NSPredicate.init(format: "NOT(hashMessage IN %@)", hashList)
        
        Message.mr_deleteAll(matching: predicate, in: context)
    }
    
    private static func chekForNewMessages(dictionaryMessages: Dictionary<String, AnyObject>, context : NSManagedObjectContext) {
        
        let arrayMessages = Array(dictionaryMessages.values)
        
        arrayMessages.forEach { (messageData) in
            
            if let hashMessage = messageData.value(forKey: "hash") as? String {
                
                let message = Message.mr_findFirst(byAttribute: "hashMessage", withValue: hashMessage, in: context)
                
                if let messageDict = messageData as? Dictionary<String, AnyObject> {
                    
                    if message == nil {
                        
                        let createMessage = Message.mr_createEntity(in: context)
                        createMessage?.setProperties(withData: messageDict)
                        
                        if createMessage?.status == 0 && createMessage != nil && createMessage?.senderID != UserManager.curentUser?.userID {
                            self.userReadMessage(dialogID: createMessage!.dialogID, hash: createMessage!.hashMessage,
                                                 userIDCome: createMessage!.senderID)
                        }
                        
                    } else {
                        message!.setProperties(withData: messageDict)
                    }
                }
            }
        }
    }

    
    
    
    
    
    
    
    
    
    
    
 
    
    
    
    
    
    
    
}
