//
//  Contact+CoreDataProperties.swift
//  
//
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact");
    }

    @NSManaged public var balance: Int64
    @NSManaged public var city: String
    @NSManaged public var code: String
    @NSManaged public var countNewMessages: Int64
    @NSManaged public var country: String
    @NSManaged public var dateLastMessage: NSDate
    @NSManaged public var dialogID: Int64
    @NSManaged public var email: String
    @NSManaged public var fio: String
    @NSManaged public var lastMessage: String
    @NSManaged public var lastVisit: String
    @NSManaged public var number: String
    @NSManaged public var online: Int64
    @NSManaged public var path: String
    @NSManaged public var dType: Int64
    @NSManaged public var userID: Int64
    @NSManaged public var dTitle: String
    @NSManaged public var dPath: String
    @NSManaged public var phone: String
}
