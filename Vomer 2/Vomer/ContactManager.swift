//
//  Chat.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import MagicalRecord

//protocol OnlineStatusDelegate: class {
//    func updateStatus(_ userID: Int64, isOnline: Bool)
//}

struct ContactManager {
    
    static let imageUrlSmall = "https://vomer.com.ua/uploads/min_"
    static let imageUrlBig = "https://vomer.com.ua/uploads/"

    
    //MARK: - From Socket
    
    static func onNewOnline(data: Array<Dictionary<String, AnyObject>>) {
        
        var type = data[0]["type"] as? Int64 ?? -1
        if type == -1 {
            type = Int64(data[0]["type"] as? String ?? "-1") ?? -1
        }
        let userID = data[0]["userID"] as? Int64 ?? -1
        
        if userID != UserManager.curentUser!.userID {
            self.updateContactOnlineStatus(userID: userID, type: type)
        }
    }
    

    
    static func onBalanceChanged(data: [Int]) {
        UserManager.curentUser?.balance = String(data[0])
    }
    
    static func onNewChat() {
        
    }
    
    //MARK: - To Socket
    
    private static func getContacts(id: Int64, completion: @escaping (_ data: Dictionary<String, AnyObject>) -> ()) {
        Socket.shared.getContacts(id: id) { (data) in
            if let array = data as? [Dictionary<String, AnyObject>] {
                if let arrayContacts = array.first {
                    completion(arrayContacts)
                }
            }
        }
    }
    
    static func deleteContact(contact: Contact) {
        Socket.shared.deleteContact(dialogID: contact.dialogID)
        self.updateAllContacts(nil)
    }
    
    //MARK: - CoreData
    
    static var allContacts: [Contact] {
        var array = [Contact]()
        for contact in Contact.mr_findAll() as! [Contact] {
            if contact.fio != "Администрация" && contact.fio != "Техническая поддержка" && contact.fio != "VOMER" && contact.dType == 0 {
                array.append(contact)
            }
        }
        return array.sorted() {$0.0.fio < $0.1.fio}
    }
    
    static var allChatsAndContactsFromServer: [Contact] {
        return Contact.mr_findAll() as! [Contact]
    }
    
    static var allSingleChats: [Contact] {
        var array = [Contact]()
        for contact in Contact.mr_findAll() as! [Contact] {
            if contact.dType == 0 {
                array.append(contact)
            }
        }
        return array.sorted() {$0.0.dateLastMessage as Date > $0.1.dateLastMessage as Date}
    }
    
    static var allGroupChats: [Contact] {
        var array = [Contact]()
        for contact in Contact.mr_findAll() as! [Contact] {
            if contact.dType == 1 {
                array.append(contact)
            }
        }
        return array.sorted() {$0.0.dateLastMessage as Date > $0.1.dateLastMessage as Date}
    }
    
    static func deleteAllContacts() {
        MagicalRecord.save({ (localContext) in
            Contact.mr_truncateAll(in: localContext)
        })
    }
    
//    static func createNewContacts(completion : @escaping ()->()) {
//        
//        self.getContacts(id: UserManager.curentUser!.userID) { (contacts) in
//            
//            MagicalRecord.save({ (localContext) in
//                contacts.forEach({ (dataResponse) in
//                    if let contactResponse = dataResponse.value as? Dictionary<String, AnyObject> {
//                        let newContact = Contact.mr_createEntity(in: localContext)
//                        newContact?.setProperties(withData: contactResponse)
//                    }
//                })
//            }) { (_, _) in
//                completion()
//            }
//        }
//    }
    
    static func updateAllContacts(_ completion : (()->())?) {
        
        self.getContacts(id: UserManager.curentUser!.userID) { (contacts) in
            MagicalRecord.save({ (localContext) in
                
                chekForDeleteUsers(dictionaryContacts: contacts, context: localContext)
                chekForNewContactsAndUpdateOld(dictionaryContacts: contacts, context: localContext)
                
            }) { (_, _) in
                completion?()
                NotificationCenter.default.post(name: .myNotificationReloadContacts, object: nil)
            }
        }
    }
    
    static private func chekForDeleteUsers(dictionaryContacts: Dictionary<String, AnyObject>, context : NSManagedObjectContext) {
        
        let arrayUsers = Array(dictionaryContacts.values)
        var idList = [Int]()
        
        arrayUsers.forEach { (obj) in
            let id = obj["dialogID"] as? Int ?? -1
            idList.append(id)
        }
        
        let predicate = NSPredicate.init(format: "NOT(dialogID IN %@)", idList)
        
        Contact.mr_deleteAll(matching: predicate, in: context)
    }
    
    static private func chekForNewContactsAndUpdateOld(dictionaryContacts: Dictionary<String, AnyObject>, context : NSManagedObjectContext) {
        
        let arrayUsers = Array(dictionaryContacts.values)
        
        arrayUsers.forEach { (userData) in
            
            if let dialogID = userData.value(forKey: "dialogID") as? Int64 {
                
                let contact = Contact.mr_findFirst(byAttribute: "dialogID", withValue: dialogID, in: context)
                
                if let userDict = userData as? Dictionary<String, AnyObject> {
                    
                    if contact == nil {
                        
                        let createContact = Contact.mr_createEntity(in: context)
                        createContact?.setProperties(withData: userDict)
                        
                    } else {
                        
                        contact!.setProperties(withData: userDict)
                    }
                }
            }
        }
    }
    
    static private func updateContactOnlineStatus(userID: Int64, type: Int64) {
        MagicalRecord.save({ (localContext) in
            let contact = Contact.mr_findFirst(byAttribute: "userID", withValue: userID, in: localContext)
            if contact != nil {
                contact!.online = type
            }
        }) { (_, _) in
            NotificationCenter.default.post(name: .myNotificationReloadContacts, object: nil)
            NotificationCenter.default.post(name: .myNotificationReloadContactStatus, object: nil, userInfo: ["userID" : userID])
        }
    }

    
}














