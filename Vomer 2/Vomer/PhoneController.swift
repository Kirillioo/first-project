//
//  PhoneController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import MBProgressHUD

class PhoneController: UIViewController, UITextFieldDelegate {
    
    var hud = MBProgressHUD()
    
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var addNumber: UITextField!
    
    var cleanNumber : String {
        return self.addNumber.text!.replacingOccurrences(of: ".", with: "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBarUI()
        
        self.addNumber.text = "+"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.addNumber.text = ""
        self.contactView.isHidden = true
    }
    
    //MARK: - Formatting Number
    
    @IBAction func nums(_ sender: UIButton) {
        let digit = sender.currentTitle!
        
        let phone: NSMutableString = NSMutableString(string: self.addNumber.text!.replacingOccurrences(of: ".", with: ""))
        
        switch phone.length {
        case 0 :
            phone.insert("+", at: 0)
            phone.append(digit)
            
        case 5, 6, 7 :
            phone.insert(".", at: 5)
            phone.append(digit)
            
        case 8, 9 :
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)
            phone.append(digit)
            
        case 10, 11 :
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)
            phone.insert(".", at: 12)
            phone.append(digit)
            
        case 12 :
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)
            phone.insert(".", at: 12)
            
        default : phone.append(digit)
        }
        
        self.addNumber.text = phone as String
    }
    
    @IBAction func deleteNum(_ sender: UIButton) {
        if (addNumber.text?.characters.count)! > 1 {
            addNumber.text  = String(addNumber.text!.characters.dropLast())
            if addNumber.text!.characters.last == Character(".") {
                addNumber.text  = String(addNumber.text!.characters.dropLast())
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func call(_ sender: Any) {
        //Router.Common.IncomingCall.show(fromViewController: self)
    }
    @IBAction func addFriendAction(_ sender: UIButton) {
        self.showHud()
        UserAPI.createSingleChat(numberUserSearch: self.cleanNumber, pinCreator: UserManager.curentUser!.pin, idCreator: UserManager.curentUser!.userID,
                                                                                                                completion: { [unowned self] (response) in
            self.hud.hide(animated: true)
                                                                                                                    
            switch response {
            case let .Success(response) :
                
                let resultString = response["result"] as? String ?? ""
                var message = String()
                
                switch resultString {
                case "new" : message = "Чат успешно создан!"
                case "update" : message = "Чат успешно восстановлен!"
                default : message = "Чат уже присутствует!"
                }
                
                _ = SweetAlert().showAlert("Приятного общения!", subTitle: message, style: .success)
                
            case .Error :
                
                _ = SweetAlert().showAlert("Что-то пошло не так")
            }
        })
    }
    
    @IBAction func searchFriendAction(_ sender: UIButton) {
        if self.addNumber.text?.characters.count != 0 {
            
            self.contactView.isHidden = true
            self.showHud()
            
            UserAPI.findUser(numberUserSearch: self.cleanNumber, pinCreator: UserManager.curentUser!.pin, idCreator: UserManager.curentUser!.userID, completion: { (response) in
                self.hud.hide(animated: true)
                
                switch response {
                case let .Success(response) :
                    var fio = response["fio"] as? String ?? ""
                    fio.characters.removeFirst()
                    fio.characters.removeLast()
                    let path = response["path"] as? String ?? ""
                    let normalPath = path.replacingOccurrences(of: "'", with: "")
                    let fullpath = URL.init(string: ContactManager.imageUrlSmall + normalPath)
                    
                    self.nameLabel.text = fio
                    self.phoneLabel.text = self.addNumber.text
                    self.photo.sd_setImage(with: fullpath, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
                    self.contactView.isHidden = false
                    
                case .Error :
                   _ = SweetAlert().showAlert("", subTitle: "Данный пользователь не существует, возможно вы ошиблись номером!", style: AlertStyle.error)
                }
            })
        }
        
    }
    
    func showHud() {
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.label.text = "Loading"
    }
    
    
    
    
}







































