import Foundation
import UIKit

class ImagePicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Properties.
    let picker = UIImagePickerController()
    var completion: ((_ image: UIImage?, _ delete: Bool) -> Void)?
    
    // MARK: - Init.
    
    override init() {
        super.init()
        picker.delegate = self
        picker.allowsEditing = true
    }
    
    // MARK: - Image Selection.
    
    func pickImage(inViewController viewController: UIViewController, withDeleteBtn: Bool = false, completion: @escaping (_ image: UIImage?, _ delete: Bool) -> Void) {
        self.completion = completion
        showDataSourceOptions(inViewController: viewController, withDeleteBtn: withDeleteBtn)
    }
    
    // MARK: - DataSource Options.
    
    fileprivate func showDataSourceOptions(inViewController viewController: UIViewController, withDeleteBtn: Bool) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        addCameraAction(actionSheet: actionSheet, viewController: viewController)
        addPhotoLibraryAction(actionSheet: actionSheet, viewController: viewController)
        addCancelAction(actionSheet: actionSheet, viewController: viewController)
        addStickerAction(actionSheet: actionSheet, viewController: viewController)
        
        if withDeleteBtn {
            addDeleteAction(actionSheet: actionSheet, viewController: viewController)
        }
        
        viewController.present(actionSheet, animated: true, completion: nil)
    }
    
    func addCameraAction(actionSheet: UIAlertController, viewController: UIViewController) {
        actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (action) -> Void in
            self.picker.sourceType = .camera
            viewController.present(self.picker, animated: true, completion: nil)
        }))
    }
    
    func addPhotoLibraryAction(actionSheet: UIAlertController, viewController: UIViewController) {
        actionSheet.addAction(UIAlertAction(title: "Выбрать из библиотеки", style: .default, handler: { (action) -> Void in
            self.picker.sourceType = .photoLibrary
            viewController.present(self.picker, animated: true, completion: nil)
        }))
    }
    
    func addCancelAction(actionSheet: UIAlertController, viewController: UIViewController) {
        actionSheet.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { (action) -> Void in
            self.picker.dismiss(animated: true, completion: nil)
        }))
    }
    
    func addDeleteAction(actionSheet: UIAlertController, viewController: UIViewController) {
        actionSheet.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (action) -> Void in
            self.completion?(nil, true)
            self.picker.dismiss(animated: true, completion: nil)
        }))
    }

    // MARK: - UIImagePickerController Delegate.
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            completion?(nil, false)
            picker.dismiss(animated: true, completion: nil)
            return
        }
        
        completion?(image, false)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        completion?(nil, false)
        picker.dismiss(animated: true, completion: nil)
    }
}
