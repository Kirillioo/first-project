//
//  LoginController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import MBProgressHUD
import MagicalRecord

class LoginController: UIViewController {
    
    @IBOutlet weak var loginTextField: MPhoneField!
    @IBOutlet weak var pinTextField: PinTextField!
    @IBOutlet weak var buttonLogIn: UIButton!
    
    weak var hud = MBProgressHUD()
    
    // MARK: - Life Cycle.

    override func viewDidLoad() {
        super.viewDidLoad()
        pinTextField.text = "6969"
        loginTextField.text = "+3000.462.07.02"
        self.buttonLogIn.isEnabled = true
        
        //prepareNavigationBarUI()
    }

    // MARK: - User Interaction.
    @IBAction func didTapEnter(_ sender: Any) {
        showHud()
        
        UserAPI.logInUserAndGetDataWith(number: loginTextField.apiString, pin: pinTextField.text!) { [unowned self] response in
            self.hud!.hide(animated: true)
            
            switch response {
                
            case let .Success(response: response) : 
            UserManager.singInUser(userData: response, completion: { [unowned self] in
                Router.MainTabBar.setRoot(fromViewController: self)
                Socket.shared.open()
            })

            case let .Error(message: msg) :
                
                self.showAlert(message: msg, title: nil)
                self.buttonLogIn.isEnabled = false
            }
        }
    }
    
    @IBAction func didTapForgotPin(_ sender: Any) {
        Router.Login.ForgotPIN.show(fromViewController: self)
    }
    
    @IBAction func didTapWebPhone(_ sender: Any) {
        Router.Login.WebPhone.show(fromViewController: self)
    }
    
    // MARK: - Changed Text fields text.
    @IBAction func editingChangedLoginTextField(_ sender: MPhoneField) {
        self.buttonLogIn.isEnabled = self.loginTextField.isValid
    }
    @IBAction func editingChangedPiTextField(_ sender: PinTextField) {
        self.buttonLogIn.isEnabled = self.loginTextField.isValid
    }
    
    // MARK: - Helps funcs.
    func showAlert(message: String?, title: String?) {
        
        let alert = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHud() {
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud!.mode = MBProgressHUDMode.indeterminate
        self.hud!.label.text = "Loading"
    }

}


    // MARK: - Insets for Fields
extension MPhoneField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 15, dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 15, dy: 0)
    }
}
class PinTextField: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 15, dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 15, dy: 0)
    }
}

















