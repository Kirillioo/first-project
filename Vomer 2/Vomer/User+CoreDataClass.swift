//
//  User+CoreDataClass.swift
//  
//
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {
    
    var fullNumber : String {
        return self.code + self.number
    }
    
    var fullNumberWithDot : String {
        let phone: NSMutableString = NSMutableString(string: self.number)
        phone.insert(".", at: 3)
        phone.insert(".", at: 6)
        return "\(self.code).\(phone)"
    }
    
    func setProperties(user: User, userData: Dictionary<String, AnyObject>) {
        
        user.pin = userData["pin"] as? Int64 ?? -1
        user.type = userData["type"] as? Int64 ?? -1
        user.userID = userData["id"] as? Int64 ?? -1
        user.code = userData["code"] as? String ?? ""
        user.number = userData["number"] as? String ?? ""
        user.fio = userData["fio"] as? String ?? ""
        user.email = userData["email"] as? String ?? ""
        user.phone = userData["phone"] as? String ?? ""
        user.birth = userData["birth"] as? String ?? ""
        user.country = userData["country"] as? String ?? ""
        user.city = userData["city"] as? String ?? ""
        user.balance = userData["balance"] as? String ?? ""
        user.language = userData["language"] as? String ?? ""
        user.path = userData["path"] as? String ?? ""
        user.newPath = userData["newPath"] as? String ?? ""
        
        user.fileName = userData["filename"] as? String ?? ""
        user.bigPath = userData["bigPath"] as? String ?? ""
    }
    
    override public var description: String {
        return "pin = \(self.pin); type = \(self.type); userID = \(self.userID); code = \(self.code); number = \(self.number); fio = \(self.fio); email = \(self.email); phone = \(self.phone); birth = \(self.birth); country = \(self.country); city = \(self.city); balance = \(self.balance); language = \(self.language); path = \(self.path); newPath = \(self.newPath)"
    }
}
