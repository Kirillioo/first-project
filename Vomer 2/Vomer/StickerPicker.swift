
//
//  StickersPicker.swift
//  Vomer
//
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import JSQMessagesViewController

extension ImagePicker {
    
    func addStickerAction(actionSheet: UIAlertController, viewController: UIViewController) {
        actionSheet.addAction(UIAlertAction(title: "Стикер", style: .default, handler: { (action) -> Void in
            
            let viewController = viewController as! ChatViewController
            
            //get Point Y for stickers collection view
            viewController.inputToolbar.contentView.textView.becomeFirstResponder()
            viewController.inputToolbar.contentView.textView.resignFirstResponder()
            let pointY = viewController.keyboardSize != nil ? viewController.view.frame.height - viewController.keyboardSize! : 400
            
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            let collectionView = StickerPicker(frame: CGRect(x: 0, y: pointY, width: viewController.view.frame.width,
                                                                        height: viewController.view.frame.height - pointY),
                                                                                            collectionViewLayout: layout)
            collectionView.viewController = viewController
            collectionView.dataSource = collectionView
            collectionView.delegate = collectionView
            collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            collectionView.backgroundColor = UIColor.white
            
            //add border
            let view = UIView(frame: CGRect(x: collectionView.frame.minX, y: collectionView.frame.minY - 1, width: collectionView.frame.width, height: 1))
            view.backgroundColor = UIColor.black
            collectionView.borderView = view
            viewController.view.addSubview(view)
            
            viewController.collectionView.contentInset.bottom = viewController.keyboardSize ?? 400
            viewController.stickerCollectionView = collectionView
            viewController.view.addSubview(collectionView)
            
    }))
    }

    
}

class StickerPicker: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var viewController = ChatViewController()
    weak var borderView = UIView()

    var stickers = [UIImage]()

    deinit {
        self.borderView?.removeFromSuperview()
    }
    
    func updateStickers() {
        let temp = 134200
        for i in 0...19 {
            if UIImage(named:"sticker_viber_00\(temp+i)") != nil {
                stickers.append(UIImage(named:"sticker_viber_00\(temp+i)")!)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if stickers.count == 0 {
            updateStickers()
        }
        return stickers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        let image = stickers[indexPath.row]
        let imageView = UIImageView.init(image: image)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        cell.backgroundView = UIImageView.init(image: image)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                                                                sizeForItemAt indexPath: IndexPath) -> CGSize {

        let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        let SCREEN_MAX_LENGTH    = max(SCREEN_WIDTH, SCREEN_HEIGHT)

        let IS_IPHONE_5          = SCREEN_MAX_LENGTH == 568.0
        
        if IS_IPHONE_5 {
            return CGSize(width: collectionView.frame.size.width / 2 - 35, height: collectionView.frame.size.height / 3 - 5)
        }
        
        return CGSize(width: collectionView.frame.size.width / 3 - 35, height: collectionView.frame.size.height / 3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if UserManager.isHaveCurrent {
//            let mediaItem = JSQPhotoMediaItem(image: self.stickers[indexPath.row])
//            let sendMessage = JSQMessageMy(senderId: String(UserManager.curentUser!.userID), displayName: UserManager.curentUser!.fio, media: mediaItem)
//            viewController.messages.append(sendMessage)
//            JSQSystemSoundPlayer.jsq_playMessageSentSound()
//            viewController.finishSendingMessage(animated: true)
//        }
    }
    
}























