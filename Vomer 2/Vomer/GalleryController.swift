//
//  GalleryController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/15/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import NYTPhotoViewer

class CustomPhoto: NSObject, NYTPhoto  {
    
    var image: UIImage?
    var imageData: Data?
    var placeholderImage: UIImage?
    let attributedCaptionTitle: NSAttributedString?
    let attributedCaptionSummary: NSAttributedString? = NSAttributedString(string: "summary string", attributes: [NSForegroundColorAttributeName: UIColor.gray])
    let attributedCaptionCredit: NSAttributedString? = NSAttributedString(string: "credit", attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
    
    init(image: UIImage? = nil, imageData: NSData? = nil, attributedCaptionTitle: NSAttributedString) {
        self.image = image
        self.imageData = imageData as Data?
        self.attributedCaptionTitle = attributedCaptionTitle
        super.init()
    }
    
}

class GalleryController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let attibutedString = NSAttributedString(string: "Supre View")
        let photosVC = NYTPhotosViewController(photos: [CustomPhoto(image: #imageLiteral(resourceName: "temp"), attributedCaptionTitle: attibutedString), CustomPhoto(image: #imageLiteral(resourceName: "temp"), attributedCaptionTitle: attibutedString), CustomPhoto(image: #imageLiteral(resourceName: "temp"), attributedCaptionTitle: attibutedString), CustomPhoto(image: #imageLiteral(resourceName: "temp"), attributedCaptionTitle: attibutedString), CustomPhoto(image: #imageLiteral(resourceName: "temp"), attributedCaptionTitle: attibutedString)])
        present(photosVC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: (Screen.width - 20) / 3, height: (Screen.width - 20) / 3)
    }
}
