    //
//  GroupsController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class GroupsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    internal var groupChats = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContactsCell), name: .myNotificationReloadContacts, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ContactManager.updateAllContacts(nil)
        self.groupChats = ContactManager.allGroupChats
        self.tableView.reloadData()
        
        if tabBarController?.tabBar.isHidden == true {
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    // MARK: - UITableView DataSource.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupsCell") as! GroupChatsTableViewCell
        
        let contact = self.groupChats[indexPath.item]
        
        cell.photo.sd_setImage(with: URL.init(string: ContactManager.imageUrlSmall + contact.dPath)!, placeholderImage: #imageLiteral(resourceName: "groupChat"))
        cell.chatNameLabel.text = contact.dTitle == "" ? contact.fio : contact.dTitle
        cell.lastMessageLabel.text = contact.lastMessage
        cell.timeLastMessageLabel.text = MyDateFormatter.getTimeStringFromDate(contact.dateLastMessage)
        
        return cell
    }
    
    // MARK: - UITableView Delegate.
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        Router.Chat.show(fromViewController: self, contactModel: self.groupChats[indexPath.item])
    }
    
    // MARK: - UpdateContactsNotification.
    
    func reloadContactsCell() {
        self.groupChats = ContactManager.allGroupChats
        self.tableView.reloadSections(IndexSet.init(integer: 0) , with: .fade)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
