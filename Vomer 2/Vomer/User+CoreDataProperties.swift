//
//  User+CoreDataProperties.swift
//  
//
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }
    
    @NSManaged public var balance: String
    @NSManaged public var bigPath: String
    @NSManaged public var birth: String
    @NSManaged public var city: String
    @NSManaged public var code: String
    @NSManaged public var country: String
    @NSManaged public var email: String
    @NSManaged public var fileName: String
    @NSManaged public var fio: String
    @NSManaged public var language: String
    @NSManaged public var newPath: String
    @NSManaged public var number: String
    @NSManaged public var path: String
    @NSManaged public var phone: String
    @NSManaged public var pin: Int64
    @NSManaged public var type: Int64
    @NSManaged public var userID: Int64

}
