//
//  Extention+NotificationName.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let myNotificationReloadContacts = Notification.Name("myNotificationReloadContacts")
    static let myNotificationReloadContactStatus = Notification.Name("myNotificationReloadContactStatus")
    static let myNotificationMessagesUpdateDidEnd = Notification.Name("myNotificationMessagesUpdateDidEnd")
}
