//
//  ContactsController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class ContactsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    fileprivate var contacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadContactsCell), name: .myNotificationReloadContacts, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.contacts = ContactManager.allContacts
        self.tableView.reloadData()
    }
    
    
    // MARK: - UITableView DataSource.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactsTableViewCell
        
        let contact = contacts[indexPath.item]
        
        cell.photo.sd_setImage(with: URL.init(string: ContactManager.imageUrlSmall + contact.path)!, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
        cell.nameLabel.text = contact.fio
        
        return cell
    }
    
    // MARK: - UITableView Delegate.
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        Router.UserDetail.show(fromViewController: self, contactModel: contacts[indexPath.item])
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let micAction = UITableViewRowAction(style: .normal, title: "Mic") { (action, indexPath) in
            tableView.isEditing = false
             Router.Common.Mic.show(fromViewController: self)

        }
        micAction.backgroundColor = UIColor.orange
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action, indexPath) in
            tableView.isEditing = false
        }
        deleteAction.backgroundColor = UIColor.red
        
        let infoAction = UITableViewRowAction(style: .normal, title: "Info") { (action, indexPath) in
            tableView.isEditing = false
            Router.UserDetail.show(fromViewController: self, contactModel: self.contacts[indexPath.item])
        }
        infoAction.backgroundColor = UIColor.green
        
        return [infoAction, deleteAction, micAction]
    }
    
    // MARK: - UpdateContactsNotification.
    
    func reloadContactsCell() {
        self.contacts = ContactManager.allContacts
        self.tableView.reloadSections( IndexSet.init(integer: 0) , with: .fade)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
}
