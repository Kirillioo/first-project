import Foundation
import Alamofire

protocol MainAPI {
    
    static func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, headers: HTTPHeaders?, completion: ServerResult?)
}

extension MainAPI {
    
    static func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, headers: HTTPHeaders?, completion: ServerResult?) {

        let urlString = url!
        
        print(urlString)
        print(parameters ?? "parameters nil")
        //print(headers ?? "headers nil")

        Alamofire.request(urlString, method: type, parameters: parameters, headers: headers).responseJSON { (response) in
            print(response)
            
            guard let response = response.result.value as? Dictionary<String, AnyObject> else {
                completion?( .Error (message: "Something goes wrong. Try again later."))
                return
            }
            
            if response["result"] as? String == "new" ||
                response["result"] as? String == "update"  ||
                    response["result"] as? String == "select"  {
                
                completion?( .Success (response: response))
                return
            }

            guard let status = response["result"] as? String, status == "done" else {
                print(response["result"] as? String ?? "response[\"result\"] = nil")
                completion?( .Error (message: response["result"] as? String))
                return
            }
            
            completion?( .Success (response: response))
        }
    }
}

