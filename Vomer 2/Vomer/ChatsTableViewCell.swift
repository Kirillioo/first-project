//
//  ChatTableViewCell.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import UIKit

class ChatsTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var unreadMessagesLabel: UILabel!
    @IBOutlet weak var timeLastMessageLabel: UILabel!
    
}
