//
//  Constants.swift
//
//  Copyright © 2016 moleculus. All rights reserved.
//

import Foundation
import UIKit

struct Screen {
    static let scale = UIScreen.main.scale
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    static let pixel = 1 / UIScreen.main.scale
    static let navbarHeight = 64
    static let tabbarHeight = 49
    static let heightWithoutNavbar = UIScreen.main.bounds.size.height - 64
}
