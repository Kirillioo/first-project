import Foundation

enum APIResponse {
    case Success (response: Dictionary<String, AnyObject>)
    case Error (message: String?)
}

typealias ServerResult = (_ response: APIResponse) -> Void
