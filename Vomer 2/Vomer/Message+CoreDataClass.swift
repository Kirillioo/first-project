//
//  Message+CoreDataClass.swift
//  
//
//
//

import Foundation
import CoreData
import JSQMessagesViewController

@objc(Message)
public class Message: NSManagedObject {

    func setProperties(withData: Dictionary<String, AnyObject>) {
        let date = withData["sorting"] as? TimeInterval ?? -1
        self.sorting = NSDate(timeIntervalSince1970: date)
        self.senderID = withData["sender_id"] as? Int64 ?? -1
        self.hashMessage = withData["hash"] as? String ?? ""
        self.dialogID = withData["dialog_id"] as? Int64 ?? -1
        self.status = withData["status"] as? Int64 ?? -1
        self.fio = withData["fio"] as? String ?? ""
        self.path = withData["path"] as? String ?? ""
        self.timeString = withData["time"] as? String ?? ""
        self.dateString = withData["date"] as? String ?? ""
        self.text = withData["text"] as? String ?? ""
        self.sendStatus = true
    }
}
