//
//  SettingsTableController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class SettingsTableController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            navigationController?.setNavigationBarHidden(true, animated: true)
            tabBarController?.tabBar.isHidden = false
        }
    }
}
