//
//  ContactsTableViewCell.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
