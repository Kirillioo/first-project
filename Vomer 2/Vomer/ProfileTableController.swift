//
//  ProfileController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/13/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class ProfileTableController: UITableViewController {

    
    //infoStaticCell
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var personalDataCell: UITableViewCell!
    @IBOutlet weak var gallaryCell: UITableViewCell!
    @IBOutlet weak var inviteFriendCell: UITableViewCell!
    @IBOutlet weak var settingsCell: UITableViewCell!
    @IBOutlet weak var logoutCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationBarUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserManager.isHaveCurrent {
            let photoPath = URL.init(string: ContactManager.imageUrlSmall + UserManager.curentUser!.path)!
            self.photo.sd_setImage(with: photoPath, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
            self.nameLabel.text = UserManager.curentUser?.fio
            self.phoneLabel.text = UserManager.curentUser?.fullNumberWithDot
            self.balanceLabel.text = "Balance now: \(UserManager.curentUser!.balance)"
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)
        
        if selectedCell == settingsCell {
            Router.UserProfile.Settings.show(fromViewController: self)
        } else if selectedCell == inviteFriendCell {
            Router.UserProfile.InviteFriends.show(fromViewController: self)
        } else if selectedCell == gallaryCell {
            Router.UserProfile.Gallery.show(fromViewController: self)
        } else if selectedCell == personalDataCell {
            Router.UserProfile.MyProfile.show(fromViewController: self)
        } else if selectedCell == logoutCell {
            ContactManager.deleteAllContacts()
            UserManager.deleteCurrentUser()
            MessageManager.deleteAllMessages()
            Router.Login.showAsRoot(fromViewController: self)
            Socket.shared.closeSocket()
        }
    }
    
    
}
