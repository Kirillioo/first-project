//
//  GroupChatsTableViewCell.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import UIKit

class GroupChatsTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var chatNameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var timeLastMessageLabel: UILabel!
    
}
