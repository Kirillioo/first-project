//
//  JSQMessageMy.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation
import JSQMessagesViewController


class JSQMessagesMy: JSQMessage {
    
    var status : Int64
    var dialogID : Int64
    var hashMessage : String
    var dateString : String
    var timeString : String
    var sorting : NSDate
    var sendStatus : Bool
    
    func setProperties(from: JSQMessagesMy) {
        self.status = from.status
        self.dialogID = from.dialogID
        self.hashMessage = from.hashMessage
        self.dateString = from.dateString
        self.timeString = from.timeString
        self.sorting = from.sorting
        self.sendStatus = from.sendStatus
    }
    
    init (withData: Dictionary<String, AnyObject>) {
        
        let timeInterval = withData["sorting"] as? TimeInterval ?? -1
        self.sorting = NSDate(timeIntervalSince1970: timeInterval)
        self.status = withData["status"] as? Int64 ?? -1
        self.dialogID = withData["dialog_id"] as? Int64 ?? -1
        self.hashMessage = withData["hash"] as? String ?? ""
        self.timeString = withData["time"] as? String ?? ""
        self.dateString = withData["date"] as? String ?? ""
        self.sendStatus = true
        
        let senderId = withData["id"] as? Int64 ?? -1
        let fio = withData["fio"] as? String ?? ""
        let date = Date(timeIntervalSince1970: timeInterval)
        let text = withData["mess"] as? String ?? ""
 
        super.init(senderId: String(senderId), senderDisplayName: fio, date: date, text: text)
    }
    
    override init(senderId: String, senderDisplayName: String, date: Date, text: String) {
        self.status = -1
        self.dialogID = -1
        self.hashMessage = ""
        self.dateString = ""
        self.timeString = ""
        self.sorting = NSDate()
        self.sendStatus = false
        super.init(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
    }
    
    override init(senderId: String, senderDisplayName: String, date: Date, media: JSQMessageMediaData) {
        self.status = -1
        self.dialogID = -1
        self.hashMessage = ""
        self.dateString = ""
        self.timeString = ""
        self.sorting = NSDate()
        self.sendStatus = false
        super.init(senderId: senderId, senderDisplayName: senderDisplayName, date: date, media: media)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
}
