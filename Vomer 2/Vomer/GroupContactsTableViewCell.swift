//
//  GroupContactsTableViewCell.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import UIKit

class GroupContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
}
