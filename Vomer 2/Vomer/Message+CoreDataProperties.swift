//
//  Message+CoreDataProperties.swift
//  
//
//
//

import Foundation
import CoreData


extension Message {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message");
    }

    @NSManaged public var dialogID: Int64
    @NSManaged public var fio: String
    @NSManaged public var hashMessage: String
    @NSManaged public var path: String
    @NSManaged public var dateString: String
    @NSManaged public var timeString: String
    @NSManaged public var status: Int64
    @NSManaged public var senderID: Int64
    @NSManaged public var sorting: NSDate
    @NSManaged public var text: String
    @NSManaged public var sendStatus: Bool

}
