//
//  ChatSettingsTableController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/21/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class ChatSettingsTableController: UITableViewController {

    @IBOutlet weak var changeFonCell: UITableViewCell!
    @IBOutlet weak var contactInfoCell: UITableViewCell!
    @IBOutlet weak var renameContactInfo: UITableViewCell!
    @IBOutlet weak var galleryCell: UITableViewCell!
    @IBOutlet weak var languageCell: UITableViewCell!
    @IBOutlet weak var trustCell: UITableViewCell!
    @IBOutlet weak var passwordCell: UITableViewCell!
    @IBOutlet weak var hideContactCell: UITableViewCell!
    @IBOutlet weak var deleteChatCell: UITableViewCell!
    
    @IBOutlet weak var trustedImage: UIImageView!
    
    var contactModel: Contact?
    
    
    let imagePicker = ImagePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.topItem?.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        if isMovingFromParentViewController {
//            tabBarController?.tabBar.isHidden = true
//        } else {
//            tabBarController?.tabBar.isHidden = false
//        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if cell == renameContactInfo {
            didTapRenameContact()
        } else if cell == deleteChatCell {
            didTapDeleteChat()
        } else if cell == passwordCell {
            didTapAddPassword()
        } else if cell == trustCell {
            trustedImage.isHidden = !trustedImage.isHidden
        } else if cell == contactInfoCell {
            Router.UserDetail.show(fromViewController: self, contactModel: self.contactModel!)
        } else if cell == galleryCell {
            Router.UserProfile.Gallery.show(fromViewController: self)
        } else if cell == languageCell {
            Router.UserProfile.Language.show(fromViewController: self)
        } else if cell == changeFonCell {
            imagePicker.pickImage(inViewController: self, withDeleteBtn: false) { (image, delete) in
                guard let image = image else { return }
                
            }
        }
    }
    
    private func didTapAddPassword() {
        let alert = UIAlertController(title: nil, message: "Введите пароль для доступа к контакту", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in })
        
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: { (action) -> Void in
            
        })
        
        okAction.isEnabled = false
        okAction.setValue(UIColor.gray, forKey: "titleTextColor")
        
        alert.addAction(okAction)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: alert.textFields![0], queue: OperationQueue.main, using: { (notification) in
            let textField = alert.textFields![0] as UITextField
            
            if textField.text!.characters.count > 0 {
                okAction.isEnabled = true
                okAction.setValue(UIColor.blue, forKey: "titleTextColor")
            } else {
                okAction.isEnabled = false
                okAction.setValue(UIColor.gray, forKey: "titleTextColor")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    private func didTapDeleteChat() {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите удалить чат?", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "ДА", style: .default, handler: { [unowned self] (_) -> Void in
            
            ContactManager.deleteContact(contact: self.contactModel!)
            _ = self.navigationController?.popToRootViewController(animated: true)
        })
        
        let noAction = UIAlertAction(title: "НЕТ", style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)

        present(alert, animated: true, completion: nil)
    }
    
    private func didTapRenameContact() {
        let alert = UIAlertController(title: nil, message: "Введите новое имя контакта", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in })
        
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: { (action) -> Void in
        
        })
        
        okAction.isEnabled = false
        okAction.setValue(UIColor.gray, forKey: "titleTextColor")
        
        alert.addAction(okAction)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: alert.textFields![0], queue: OperationQueue.main, using: { (notification) in
            let textField = alert.textFields![0] as UITextField
            
            if textField.text!.characters.count > 0 {
                okAction.isEnabled = true
                okAction.setValue(UIColor.blue, forKey: "titleTextColor")
            } else {
                okAction.isEnabled = false
                okAction.setValue(UIColor.gray, forKey: "titleTextColor")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func didTapLanguage() {
        
}
}
