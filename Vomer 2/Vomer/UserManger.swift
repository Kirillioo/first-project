//
//  UserManger.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation
import MagicalRecord

struct UserManager {
    
    static var curentUser: User? {
        return User.mr_findFirst()
    }
    
    static var isHaveCurrent: Bool {
        if self.curentUser != nil {
            return true
        }
        return false
    }

    static func singInUser(userData: Dictionary<String, AnyObject>, completion : @escaping ()->()) {
        MagicalRecord.save({ (localContext) in
            let newUser = User.mr_createEntity(in: localContext)
            newUser?.setProperties(user: newUser!, userData: userData)
        }) { (_, _) in
            completion()
        }
    }
    
    static func deleteCurrentUser() {
        MagicalRecord.save({ (localContext) in
            let user = self.curentUser?.mr_(in: localContext)
            user?.mr_deleteEntity()
        })
    }
    
    static func changeUserInfoWith(type: Int64?, userID: Int64?, code: String?, fio: String?, email: String?, birth: String?,
                            country: String?, city: String?, balance: String?, language: String?, path: String?, newPath: String?, completion : @escaping ()->()) {
        MagicalRecord.save({ (localContext) in
            let user = curentUser?.mr_(in: localContext)
            
            if type != nil {
                user?.type = type!
            }
            if userID != nil {
                user?.userID = userID!
            }
            if code != nil {
                user?.code = code!
            }
            if fio != nil {
                user?.fio = fio!
            }
            if email != nil {
                user?.email = email!
            }
            if birth != nil {
                user?.birth = birth!
            }
            if country != nil {
                user?.country = country!
            }
            if city != nil {
                user?.city = city!
            }
            if balance != nil {
                user?.balance = balance!
            }
            if language != nil {
                user?.language = language!
            }
            if path != nil {
                user?.path = path!
            }
            if newPath != nil {
                user?.newPath = newPath!
            }
            
        }) { (_, _) in
            completion()
        }
    }

}
