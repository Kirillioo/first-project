//
//  io.swift
//  skytaxi
//
//  Created by Oleg Melnik on 25.05.16.
//  Copyright © 2016 Moleculus. All rights reserved.
//

import Foundation
import SocketIO


class Socket {
    
    static let shared = Socket()
    
    let socket = SocketIOClient(socketURL: URL(string: "https://my.vomer.com.ua:3301")!, config: [.log(true), .forcePolling(false)])
    
    func open() {
        if UserManager.isHaveCurrent {
            socket.once("getData") { data, _ in
                
                self.socket.emitWithAck("Vomer syncLogin", "\(UserManager.curentUser!.userID)", "\(UserManager.curentUser!.number)",
                                                                                                        "\(UserManager.curentUser!.pin)", "2") (0) { data in
                   ContactManager.updateAllContacts(nil)
                 }
            }
            
            socket.connect(timeoutAfter: 4, withHandler: {
                let alert = NoConnectionToServerAlert.init()
                alert.show()
            })
            
            socket.on("vomer message") { (data, _) in
                MessageManager.getNewMessage(data: data)
            }
            
            socket.on("vomer keyup") { (data, _) in
                MessageManager.onWritesMessage(data: data)
            }
            
            socket.on("vomer ReadMessage") { (data, _) in
                MessageManager.onReadMessage(data: data)
            }
            
            socket.on("vomer ReadMessageAll") { (data, _) in
                //print(data)
            }
            
            socket.on("vomer NowBalance") { (data, _) in
                ContactManager.onBalanceChanged(data: data as! Array<Int>)
            }

            socket.on("vomer online") { (data, _) in
                ContactManager.onNewOnline(data: data as! Array<Dictionary<String, AnyObject>>)
            }
            
            socket.on("vomer onNewChat") { (data, _) in
                ContactManager.updateAllContacts(nil)
            }

        }
    }
    
    func sendMessage(messageText: String, toID: Int64) {
        socket.emitWithAck("vomer message", messageText, "", Int(toID), 0)(0) { _ in }
    }
    
    func userWhriteMessage(dialogID: Int64) {
        socket.emit("vomer keyup", "", Int(dialogID), 0);
    }
    
    func userReadMessage(dialogID: Int64, hash: String, userIDCome: Int64) {
        socket.emit("vomer ReadMessage", Int(dialogID), hash, Int(userIDCome))
    }

    func getAllMessages(dialogID: Int64, completion: @escaping (Any) -> ()){
        socket.emitWithAck("vomer getAllMessage", Int(dialogID), "") (0) { data in
            completion(data)
        }
    }
    
    func getContacts(id: Int64, completion: @escaping (Any) -> ()) {
        socket.emitWithAck("vomer GetContactIphone", Int(id)) (0) { data in
            completion(data)
        }
    }
    
    func deleteContact(dialogID: Int64) -> () {
        socket.emitWithAck("vomer DeleteContact", Int(dialogID)) (0) { data in
            print(data)
        }
    }

    
    func closeSocket() {
        socket.removeAllHandlers()
        socket.disconnect()
    }
    
    func isConnecting() -> Bool {
        return socket.status == SocketIOClientStatus.connecting
    }
    
    func isConnected() -> Bool {
       return socket.status == SocketIOClientStatus.connected
    }
    
    func isDisconected() -> Bool {
        return socket.status == SocketIOClientStatus.disconnected || socket.status == SocketIOClientStatus.notConnected
    }

}
