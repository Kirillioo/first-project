//
//  CreateGroupChatController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 1/12/17.
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import UIKit
import MBProgressHUD

class CreateGroupChatController: UIViewController, SelectDeselectDelegate, UITextFieldDelegate {

    @IBOutlet weak var nameChatTextField: PinTextField!
    @IBOutlet weak var createButton: UIBarButtonItem!
    
    var hud = MBProgressHUD()
    
    var usersArray = [String]()
    
    @IBAction func createButtonDidTap(_ sender: Any) {
        self.showHud()
        var stringUsersID = String()
        for id in self.usersArray {
            id == usersArray.last ? stringUsersID.append("\(id)") : stringUsersID.append("\(id),")
        }
        UserAPI.createGroupChat(usersForChat: stringUsersID, pinCreator: UserManager.curentUser!.pin, idCreator: UserManager.curentUser!.userID,
                                nameChat: self.nameChatTextField.text!) { [unowned self] (response) in
                                    
                                    self.hud.hide(animated: true)
                                    
                                    switch response {
                                    case .Success :
                                        
                                        ContactManager.updateAllContacts(nil)
                                        
                                        _ = SweetAlert().showAlert("Приятного общения", subTitle: "Чат успешно создан!", style: .success,
                                                                   buttonTitle: "Ok", action: { (_) in

                                            _ = self.navigationController?.popToRootViewController(animated: true)
                                        })
                                        
                                    case .Error :
                                        
                                        _ = SweetAlert().showAlert("Error!", subTitle: "Пожалуйста, проверте свое соединение с интернетом.", style: .error)
                                    }
                                    
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tableViewContainerGroupChatCreate" {
            let firstVC = segue.destination as! GroupChatController
            firstVC.selectDeselectDelegate = self
        }
    }
    
    func someoneDidSelect(userID: String) {
        self.usersArray.append(userID)
        print(self.usersArray)
        
        self.chekForEnableButton()
    }
    
    func someoneDidDeselect(userID: String) {
        for (index, id) in self.usersArray.enumerated() {
            if id == userID {
                self.usersArray.remove(at: index)
            }
        }
        self.chekForEnableButton()
    }
    
    // MARK: - UITextFieldDelegate.
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nameChatTextField.resignFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.chekForEnableButton()
        if range.location == 0 && string == "" {
            self.createButton.isEnabled = false
        }
        return true
    }
    
    func chekForEnableButton() {
        if self.nameChatTextField.text!.characters.count > 0 && self.usersArray.count > 0 {
            self.createButton.isEnabled = true
        } else {
            self.createButton.isEnabled = false
        }
    }
    
    // MARK: - ShowHUD.
    
    func showHud() {
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.label.text = "Loading"
    }
    
    
    
    
    
    
    
    
    
}
