//
//  DateFormat.swift
//  Vomer
//
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation

struct MyDateFormatter {
    static func getDateStringFromDate(_ date: NSDate?) -> String? {
        if date != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            return dateFormatter.string(from: date! as Date)
        }
        return nil
    }
    
    static func getTimeStringFromDate(_ date: NSDate?) -> String? {
        if date != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            return dateFormatter.string(from: date! as Date)
        }
        return nil
    }
    
    static func convertToDateFromServerString(time: String, date: String) -> NSDate {
        let str = "\(date) \(time)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        let date = dateFormatter.date(from: str)
        
        return date! as NSDate
    }
}
