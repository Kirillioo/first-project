import Foundation
import UIKit
import Alamofire

struct API {
    
    static let loginAndChangeData = "https://my.vomer.com.ua:3301/android"
    static let createChat = "https://my.vomer.com.ua:3301/CreateChat"
    static let uploadFile = "https://vomer.com.ua/api/uploadFile.php"
}


struct UserAPI: MainAPI {
    
    // MARK: - Login and change data.
    static func loginUserWith(number: String, pin: String, completion: ServerResult?) {
        let parameters : [String : Any] = [
            "key": "e65837978381fdb0634e294698dca5d6",
            "func": "login",
            "number": number,
            "pin": pin,
        ]
        sendRequest(type: .post, url: API.loginAndChangeData, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    static func logInUserAndGetDataWith(number: String, pin: String, completion: ServerResult?) {
        let parameters : [String : Any] = [
            "key": "e65837978381fdb0634e294698dca5d6",
            "func": "loginData",
            "number": number,
            "pin": pin,
            "token" : "token",
            "device" : "iphone"
        ]
        sendRequest(type: .post, url: API.loginAndChangeData, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    static func changeUserData(number: String, pin: String, fio: String, phone: String, birth: String, country: String, city: String, email: String,
                                                                                            path: String , id: String, completion: ServerResult?) {
        let parameters : [String : Any] = [
            "key": "e65837978381fdb0634e294698dca5d6",
            "func": "fullDataChange",
            "id": id,
            "number": number,
            "pin": pin,
            "fio": fio,
            "phone": phone,
            "birth": birth,
            "country": country,
            "city": city,
            "email" : email,
            "path" : path
        ]
        sendRequest(type: .post, url: API.loginAndChangeData, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    // MARK: - Create chats and find users.
    static func findUser(numberUserSearch: String, pinCreator: Int64, idCreator: Int64, completion: ServerResult?) {
        let parameters : [String : Any] = [
            "key": "e65837978381fdb0634e294698dca5d6",
            "func": "findUser",
            "userID": Int(idCreator),
            "number": numberUserSearch,
            "pin": Int(pinCreator)
        ]
        sendRequest(type: .post, url: API.createChat, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    static func createSingleChat(numberUserSearch: String, pinCreator: Int64, idCreator: Int64, completion: ServerResult?) {
        
        let parameters : [String : Any] = [
            "key": "e65837978381fdb0634e294698dca5d6",
            "func": "createSingleChat",
            "userID": idCreator,
            "number": numberUserSearch,
            "pin": pinCreator,
            "nameChat": ""
        ]
        sendRequest(type: .post, url: API.createChat, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    static func createGroupChat(usersForChat: String, pinCreator: Int64, idCreator: Int64, nameChat: String, completion: ServerResult?) {
        let parameters : [String : Any] = [
            "key": "e65837978381fdb0634e294698dca5d6",
            "func": "createGroupChat",
            "userID": idCreator,
            "usersForChat": usersForChat,
            "pin": pinCreator,
            "nameChat": nameChat
        ]
        sendRequest(type: .post, url: API.createChat, parameters: parameters as [String : AnyObject]?, headers: nil, completion: completion)
    }
    
    // MARK: - Upload Files.
    // TODO: - todo
    static func uploadUserPhoto(userID: Int64, image: UIImage, completion: @escaping (_ filePath: String?) -> ()) {
        
        let url = try! URLRequest(url: URL(string:"https://vomer.com.ua/api/uploadApi.php")!, method: .post, headers: nil)

        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append((String(userID)).data(using: .utf8)!, withName: "description")
            multipartFormData.append(UIImageJPEGRepresentation(image, 1)!, withName: "picture", fileName: "picture.jpg", mimeType: "picture/jpg")
        }, with: url, encodingCompletion: {
            encodingResult in
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let value = response.result.value as? Dictionary<String, AnyObject> {
                        if let fileName = value["file_name"] as? String {
                            completion(fileName)
                        }
                    }
                }
                
            case .failure:
                completion(nil)
            }
        })
    }

}
