//
//  UserDetailTableController.swift
//  Vomer
//
//  Created by Anton Vodolazkyi on 11/21/16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class UserDetailTableController: UITableViewController {

    var contactModel : Contact?

    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var fioTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.navigationController!.navigationBar.topItem?.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let bigPhotoPath = URL.init(string: ContactManager.imageUrlBig + contactModel!.path)!
        self.userPhoto.sd_setImage(with: bigPhotoPath) { [unowned self] (image, error, _, _) in
            if image == nil {
                let smallPhotoPath = URL.init(string: ContactManager.imageUrlSmall + self.contactModel!.path)!
                self.userPhoto.sd_setImage(with: smallPhotoPath, placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
            }
        }
        self.numberTextField.text = contactModel?.phone
        self.fioTextField.text = contactModel?.fio
        self.countryTextField.text = contactModel?.country
        self.cityTextField.text = contactModel?.city
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
//        if self.isMovingFromParentViewController {
//            navigationController?.setNavigationBarHidden(true, animated: true)
//        }
    }
    
}
