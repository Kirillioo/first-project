//
//  StickersPicker.swift
//  Vomer
//
//  Created by Stas on 30.12.16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit
import JSQMessagesViewController

extension ImagePicker {
    
    func addStickerAction(actionSheet: UIAlertController, viewController: UIViewController) {
        actionSheet.addAction(UIAlertAction(title: "Sticker", style: .default, handler: { (action) -> Void in
            
            viewController.view.endEditing(false)
            
            let viewController = viewController as! ChatViewController
            
            if viewController.stickerCollectionView != nil {
                
                viewController.stickerCollectionView?.removeFromSuperview()
                viewController.stickerCollectionView?.reloadData()
                viewController.view.addSubview(viewController.stickerCollectionView!)
                
            }else {

                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

                let pointY = viewController.keyboardSize != nil ? viewController.view.frame.height - viewController.keyboardSize! : 400
                
                let collectionView = StickerPicker(frame: CGRect(x: 0, y: pointY, width: viewController.view.frame.width,
                                                                 height: viewController.view.frame.height - pointY),
                                                   collectionViewLayout: layout)
                collectionView.viewController = viewController
                collectionView.dataSource = collectionView
                collectionView.delegate = collectionView
                collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
                collectionView.backgroundColor = UIColor.orange
                
                viewController.view.addSubview(collectionView)
            }
            
            
            
            //viewController.view.subviews[1].frame = CGRect(x: 0, y: 200, width: viewController.view.frame.width,height: 50)
            //viewController.view.bringSubview(toFront: viewController.view.subviews[1])
    }))
    }

    
}

class StickerPicker: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var viewController = ChatViewController()
    
    var stickers = [UIImage]()
    
    
    func updateStickers() {
        let temp = 134200
        for i in 0...19 {
            if UIImage(named:"sticker_viber_00\(temp+i)") != nil {
                stickers.append(UIImage(named:"sticker_viber_00\(temp+i)")!)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if stickers.count == 0 {
            updateStickers()
        }
        return stickers.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        let image = stickers[indexPath.row]
        cell.backgroundColor = UIColor.black
        cell.backgroundView = UIImageView.init(image: image)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                                                                sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let mediaItem = JSQPhotoMediaItem(image: self.stickers[indexPath.row])
        let sendMessage = JSQMessage(senderId: viewController.senderId, displayName: viewController.senderDisplayName, media: mediaItem)
        viewController.messages.append(sendMessage)
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        viewController.finishSendingMessage(animated: true)
    }
    
}























