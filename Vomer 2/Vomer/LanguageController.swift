//
//  LanguageController.swift
//  Vomer
//
//  Created by Lora Kucher on 07.12.16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class LanguageController: UITableViewController {
    
    var arrayWithLanguages = ["Английский", "Албанский", "Амхарский", "Арабский", "Армянский", "Белорусский", "Болгарский", "Украинский", "Польский"]

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayWithLanguages.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = arrayWithLanguages[indexPath.row]
        return cell
        }
    
  

}
