
import UIKit

// TODO: - Improve.
class MPhoneField: UITextField {
    
    // MARK: - Vars.
    
    var isValid: Bool {
        get {
            return apiString.characters.count > 11 ? true : false
        }
    }

    var cleanString: String {
        get {
            var phone = self.text
            
            phone = phone!.replacingOccurrences(of: ".", with: "")
            phone = phone!.replacingOccurrences(of: "+", with: "")

            return phone!
        }
    }

    var apiString: String {
        get {
            var phone = self.text
            
            phone = phone!.replacingOccurrences(of: ".", with: "")
            
            return phone!
        }
    }

    // MARK: - Lifecycle.

    required init!(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        NotificationCenter.default.addObserver(self, selector: #selector(phoneFormatting), name:NSNotification.Name.UITextFieldTextDidChange, object: self)
        NotificationCenter.default.addObserver(self, selector: #selector(defaultString), name:NSNotification.Name.UITextFieldTextDidBeginEditing, object: self)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Formatting.

    func defaultString() {
        self.text = "+"
    }

    func phoneFormatting() {
        var phone: NSMutableString = NSMutableString(string: self.cleanString)
        
        phone.insert("+", at: 0)
        
        switch phone.length {
        case 0: break
        case 1: break
        case 2: break
        case 3: break
        case 4: break
        case 5: break
            
        case 6:
            phone.insert(".", at: 5)
            
        case 7:
            phone.insert(".", at: 5)

        case 8:
            phone.insert(".", at: 5)

        case 9:
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)

        case 10:
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)

        case 11:
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)
            phone.insert(".", at: 12)
            
        case 12:
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)
            phone.insert(".", at: 12)
            phone = NSMutableString(string: phone.substring(to: 15))
            
        default:
            phone.insert(".", at: 5)
            phone.insert(".", at: 9)
            phone.insert(".", at: 12)
            phone = NSMutableString(string: phone.substring(to: 15))
            
        }
        
        self.text = phone as String

    }

}
