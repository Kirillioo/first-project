//
//  Extension+ViewController.swift
//  InterAd
//
//  Created by Anton Vodolazkyi on 10/30/16.
//  Copyright © 2016 moleculus. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func prepareNavigationBarUI() {
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.7638709545, blue: 0.2683038414, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]

        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: " Back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: " Back")
        self.navigationController?.navigationBar.shadowImage = #colorLiteral(red: 0.7843137255, green: 0.7803921569, blue: 0.8, alpha: 0.5).as1ptImage()
        self.navigationController?.navigationBar.setBackgroundImage(#colorLiteral(red: 0, green: 0.7638709545, blue: 0.2683038414, alpha: 1).as1ptImage(), for: .default)
    }
    
    func dismissVC() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    static func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if rootVC?.presentedViewController == nil {
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
    }
}
