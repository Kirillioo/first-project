//
//  SocketManager.swift
//  Vomer
//
//  Created by Stas on 06.01.17.
//  Copyright © 2017 Vodolaz. All rights reserved.
//

import Foundation
import SocketIO
import JSQMessagesViewController


class SocketManager {
    
    static let sharedSocket = SocketManager()
    
    var connected = false
    
    
    let socket = SocketIOClient(socketURL: URL(string: "https://my.vomer.com.ua:3301")!, config: [.log(true), .forcePolling(true)])

    func open(){
        socket.on("getData") { data, ack in
            print("socket connected")
            print(data)
            self.connected = true
            
            //201808 4620701 6969
            self.socket.emitWithAck("Vomer syncLogin", "\(UserManager.curentUser!.userId)", "\(UserManager.curentUser!.number!)",
                                                                                                    "\(UserManager.curentUser!.pin)", "2")(10) { data in
                print(data[0])
            }
            
            self.socket.on("vomer message") { (data, ack) in
                print("socket connected")
                print(data)
                self.getNewMessageFromData(data: data)
            }

        }
        
        
        
//        self.socket.on("vomer onNewChat") { (data, ack) in
//            print(data)
//        }

//                    self.socket.on("vomer NowBalance") { (data, ack) in
//                        print(data)
//                    }
//        
//                    self.socket.on("vomer online") { (data, ack) in
//                        print(data)
//                    }
//        
        
        socket.connect()
    }
    
    
    
    func getAllMessages(){
        //self.socket.emit("vomer getAllMessage", dialogID, sort, new Ack() {
    }

    func sendMessage(toID: String, withText: String) {
        self.socket.emitWithAck("vomer message", withText, "", toID, 0)(10) { data in
            print(data)
        }
    }
    
    func getContacts(id: String, complition: (Dictionary<String, AnyObject>) -> ()) {
        self.socket.emitWithAck("vomer GetContactIphone", id)(10) { data in
            print(data[0])
        }
    }
    
//    func dialogID() {
//       UserAPI.createSingleChat(numberUserSearch: "+30004620703", pinCreator: "6969", idCreator: "\(UserManager.curentUser!.userId)", nameChat: "asdasd") { (response) in
//        switch response {
//        case let .Success(response: response) :
//            print(response)
//            
//        default : print("\nfuck")
//        }
//        
//        }
//    }
    
    func getNewMessageFromData(data: Any) {
        let dataArray = data as! Array<Dictionary<String, AnyObject>>
        
        let messageSend = JSQMessage(senderId: dataArray[0]["fio"] as! String!, displayName: dataArray[0]["fio"] as! String!, text: dataArray[0]["mess"] as! String!)
        ChatViewController.chatViewController?.messages.append(messageSend)
        ChatViewController.chatViewController?.finishReceivingMessage(animated: true)
    }

}





























