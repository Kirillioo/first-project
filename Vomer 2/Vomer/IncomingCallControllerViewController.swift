//
//  IncomingCollControllerViewController.swift
//  Vomer
//
//  Created by Lora Kucher on 03.12.16.
//  Copyright © 2016 Vodolaz. All rights reserved.
//

import UIKit

class IncomingCallControllerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    @IBAction func videoCall(_ sender: Any) {
        Router.Common.VideoCall.show(fromViewController: self)
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       // Router.Common.Back.show(fromViewController: self)
    }

}
