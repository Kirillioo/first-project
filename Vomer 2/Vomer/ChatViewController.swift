//
//  ChatViewController.swift
//  InterAd
//
//  Created by Anton Vodolazkyi on 11/6/16.
//  Copyright © 2016 moleculus. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SDWebImage

class ChatViewController: JSQMessagesViewController, UIGestureRecognizerDelegate, ChatManagerDelegate {
    
    //var messages = [JSQMessagesMy.init(senderId: "123", displayName: "123", text: "suka")]
    var messages = [JSQMessagesMy]()
    
    var incomingBubble: JSQMessagesBubbleImage?
    var outgoingBubble: JSQMessagesBubbleImage?
    var timerForHidingTypingUndicator: Timer?

    let sender = UserManager.curentUser!.userID
    var contactModel : Contact?
    
    var keyboardSize : CGFloat?
    weak var stickerCollectionView: StickerPicker?
    
    var subtitleLabel: UILabel!
    
    private var btn_send: UIButton!
    private let imagePicker = ImagePicker()
    
    //test
//    deinit {
//        print("test")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MessageManager.delegate = self

        let callbarButtonView = UIImageView(frame:  CGRect(x: 0, y: 0, width: 25, height: 25))
        callbarButtonView.image = UIImage(named: "phone")
        let callbarButton = UIBarButtonItem(customView: callbarButtonView)
        
        let videobarButtonView = UIImageView(frame: CGRect(x: 30, y: 0, width: 25, height: 25))
        videobarButtonView.image = UIImage(named: "video_camera-256")
        let videobarButton = UIBarButtonItem(customView: videobarButtonView)
        
        let settingsButton = UIButton(frame: CGRect(x: 55, y: 0, width: 25, height: 25))
        settingsButton.addTarget(self, action: #selector(self.didTapChatSettings), for: .touchUpInside)
        settingsButton.setImage(UIImage(named: "setting"), for: .normal)
        
        let settingsbarButton = UIBarButtonItem(customView: settingsButton)
        
        navigationItem.rightBarButtonItems = [settingsbarButton, callbarButton, videobarButton];
        
        self.collectionView?.register(UINib(nibName: "JSQMessagesCollectionViewCellIncomingCustom", bundle: nil), forCellWithReuseIdentifier: "JSQIncoming")
        self.collectionView?.register(UINib(nibName: "JSQMessagesCollectionViewCellOutgoingCustom", bundle: nil), forCellWithReuseIdentifier: "JSQOutgoing")
        
        self.incomingCellIdentifier = "JSQIncoming"
        self.outgoingCellIdentifier = "JSQOutgoing"

        collectionView?.collectionViewLayout = CustomCollectionViewFlowLayout()
        
        btn_send = inputToolbar.contentView.rightBarButtonItem
        btn_send.setTitle("", for: .normal)
        btn_send.setImage(#imageLiteral(resourceName: "keyboardSend"), for: .normal)
        inputToolbar.contentView.textView.placeHolder = "Введите сообщение"
        inputToolbar.contentView.textView.tintColor = #colorLiteral(red: 0, green: 0.7638709545, blue: 0.2683038414, alpha: 1)
        inputToolbar.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 0.7)

        inputToolbar.contentView.leftBarButtonItem = JSQMessagesToolbarButtonFactory.defaultAccessoryButtonItem()

        incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: #colorLiteral(red: 0, green: 0.7638709545, blue: 0.2683038414, alpha: 1))
        
        collectionView?.collectionViewLayout.springinessEnabled = false
        
        self.collectionView?.layoutIfNeeded()
        self.collectionView?.reloadData()
        
        self.scrollToBottom(animated: false)
        
        self.senderId = String(UserManager.curentUser!.userID)
        self.senderDisplayName = UserManager.curentUser!.fio
        
        navigationItem.titleView = setTitle(title: self.contactModel!.fio, subtitle: self.contactModel!.online > 0 ?
                                                                                                            "online" :
                                                                                                            "last seen \(self.contactModel!.lastVisit)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = true
        
        //get and update messages
        self.messages = MessageManager.getAllMessages(self.contactModel!.dialogID)
        MessageManager.updateMessagesInBase(self.contactModel!.dialogID)
        
        //notifications
        NotificationCenter.default.addObserver(self, selector: #selector(updateStatus(notification:)), name: .myNotificationReloadContactStatus, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(messagesUpdateDidEnd), name: .myNotificationMessagesUpdateDidEnd, object: nil)


        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        //gesture
        addTapGestures()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        invalidateTimer()
        
        NotificationCenter.default.removeObserver(self)
        
        if isMovingFromParentViewController {
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let message = JSQMessagesMy(senderId: senderId, displayName: senderDisplayName, text: text)
        self.messages.append(message!)
        
        MessageManager.sendMessage(message: message!, dialogID: self.contactModel!.dialogID)
        
        self.btn_send.isEnabled = true
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        self.finishSendingMessage(animated: true)
        
        if collectionView.isAtBottom {
            scrollToBottom(animated: true)
        }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        self.view.endEditing(true)
        imagePicker.pickImage(inViewController: self, withDeleteBtn: false) { (image, delete) in
            guard let image = image else { return }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let message = messages[indexPath.item]
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        cell.cellBottomLabel.textInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 35)
        
        //cell.avatarImageView.image = JSQMessagesAvatarImageFactory.circularAvatarImage(#imageLiteral(resourceName: "first"), withDiameter: 48)
        cell.avatarImageView.layer.cornerRadius = 16
        cell.avatarImageView.clipsToBounds = true
        
        //cell.textView.text = message.text
        
        cell.textView.textColor = messages[indexPath.item].senderId == String(sender) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        if message.senderId == String(sender) && indexPath.item == messages.count - 1 {
            if message.sendStatus {
                cell.cellBottomLabel.text = "Отправлено"
            }
            if message.status > 1 {
                cell.cellBottomLabel.text = "Просмотрено"
            }
        }

        if message.senderId! == String(sender) {
            let cell = cell as! JSQMessagesCollectionViewCellOutgoingCustom
            
            cell.timeLabel.text = message.timeString
            
            cell.avatarImageView.sd_setImage(with: URL.init(string: ContactManager.imageUrlSmall + UserManager.curentUser!.path), placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
            
            return cell
        }else {
            let cell = cell as! JSQMessagesCollectionViewCellIncomingCustom
            
            cell.timeLabel.text = message.timeString
            
            cell.avatarImageView.sd_setImage(with: URL.init(string: ContactManager.imageUrlSmall + self.contactModel!.path), placeholderImage: #imageLiteral(resourceName: "placeholderAvatar"))
            
            return cell
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        return messages[indexPath.item].senderId! == String(sender) ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return messages[indexPath.item].senderId! == String(sender) ? 25 : 0
    }
    
    func setTitle(title:String, subtitle:String) -> UIView {
        let with = UIScreen.main.bounds.size.width - 135
        
        let titleLabel = UILabel(frame: CGRect(x: -15, y: -2, width: 0, height: 0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        subtitleLabel = UILabel(frame: CGRect(x: -15, y: 18, width: 0, height: 0))
        subtitleLabel.backgroundColor = UIColor.clear
        
        subtitleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6971738674)
        subtitleLabel.font = UIFont.systemFont(ofSize: 12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: with, height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        return titleView
    }
    
    // MARK: - MessagesNotifications.
    
    func messagesUpdateDidEnd() {
        self.messages = MessageManager.getAllMessages(self.contactModel!.dialogID)
        self.collectionView.reloadData()
        
        if self.collectionView.isAtBottom {
            scrollToBottom(animated: false)
        }
    }
    
    func updateStatus(notification: Notification) {
        let info = notification.userInfo
        if let userID = info?["userID"] as? Int64 {
            if userID == self.contactModel?.userID {
                navigationItem.titleView = setTitle(title: self.contactModel!.fio,
                                                    subtitle: self.contactModel!.online > 0 ? "online" :
                                                                "last seen \(self.contactModel!.lastVisit)")
            }
        }
    }
    
    // MARK: - Messages + Socket.
    
//    func reloadCollectionView() {
//        self.messages = MessageManager.getAllMessages(self.contactModel!.dialogID)
//        self.collectionView.reloadData()
//        
//        if self.collectionView.isAtBottom {
//            scrollToBottom(animated: false)
//        }
//    }
    
    func messageDidSend(myMessage: JSQMessagesMy) {
        for message in self.messages.reversed() {
            if message.text == myMessage.text {
                message.setProperties(from: myMessage)
                self.collectionView.reloadData()
                break
            }
        }
    }
    
    internal func onReadMessage(hashMessage: String, dialogID: Int, userID: Int) {
        for message in self.messages.reversed() {
            if message.hashMessage == hashMessage {
                message.status = 2
                self.collectionView.reloadData()
                break
            }
        }
    }
    
    internal func onNewMessage(message: JSQMessagesMy) {
        if message.dialogID == self.contactModel!.dialogID {
            MessageManager.userReadMessage(dialogID: self.contactModel!.dialogID, hash: message.hashMessage,
                                           userIDCome: Int64(message.senderId)!)
            
            ContactManager.updateAllContacts(nil)
            
            self.messages.append(message)
            self.finishReceivingMessage(animated: true)
            
            if collectionView.isAtBottom {
                self.scrollToBottom(animated: true)
            }
        }
    }
    
    // MARK: - Typing + Socket.
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        MessageManager.userWhriteMessage(dialogID: self.contactModel!.dialogID)
    }
    
    internal func onWritesMessage(dialogID: Int, fio: String) {
        if dialogID == Int(self.contactModel!.dialogID) {
            invalidateTimer()
            timerForHidingTypingUndicator = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.hideTypingIndicator), userInfo: nil, repeats: false)
            
            self.showTypingIndicator = true
            
            if collectionView.isAtBottom {
                self.scrollToBottom(animated: true)
            }
        }
    }
    
    @objc fileprivate func hideTypingIndicator() {
        self.showTypingIndicator = false
    }
    
    func invalidateTimer() {
        if timerForHidingTypingUndicator != nil {
            timerForHidingTypingUndicator?.invalidate()
            timerForHidingTypingUndicator = nil
        }
    }
    
    
    
    // MARK: - tapGestures.
    func addTapGestures() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapAndHideStickersView(gesture:)))
        gesture.delegate = self
        self.collectionView.addGestureRecognizer(gesture)
    }
    
    func tapAndHideStickersView(gesture: UITapGestureRecognizer) {
        if self.stickerCollectionView != nil {
            if self.view.subviews.contains(self.stickerCollectionView!) {
                self.stickerCollectionView!.removeFromSuperview()
                self.collectionView.contentInset.bottom = self.inputToolbar.frame.size.height
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    // MARK: - keyboardWillShow.
    
    func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.keyboardSize = CGFloat(keyboardSize.height)
        }
    }
    
    // MARK: - User Interaction.
    
    func didTapChatSettings() {
        Router.Chat.ChatSettings.show(fromViewController: self, contactModel: self.contactModel)
    }

}

class CustomCollectionViewFlowLayout: JSQMessagesCollectionViewFlowLayout {
    
    override func messageBubbleSizeForItem(at indexPath: IndexPath!) -> CGSize {
        var superSize = super.messageBubbleSizeForItem(at: indexPath)
        
        superSize = CGSize(width: superSize.width, height: superSize.height + 14)

        return superSize
    }
}
